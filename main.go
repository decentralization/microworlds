package main

import (
	"flag"
	"git.openprivacy.ca/sarah/microworlds/actors"
	"git.openprivacy.ca/sarah/microworlds/core"
	"git.openprivacy.ca/sarah/microworlds/graphics"
	"github.com/veandco/go-sdl2/sdl"
	"log"
	"math/rand"
	"os"
	"runtime/pprof"
	"sync"
	"time"
)


var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

var model = flag.String("model", "slime", "slimemold|swarm|woodchips")

var width = flag.Int("width", 300, "width of environment")
var height = flag.Int("height", 300, "height of environment")
var numTurtles = flag.Int("numTurtles", 5000, "number of turtles")



func main() {

	// We don't need real randomness
	rand.Seed(time.Now().Unix())

	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}
	defer sdl.Quit()


	env := core.NewEnvironment(*width,*height)
	turtles := make([]*core.Turtle,*numTurtles)


	switch *model {

	case "swarm":
		// Create 2 food blocks
		for x:= 100;x<110;x++ {
			for y:= 100;y<110;y++ {
				env.PutValue(x,y)
			}
		}
		for x:= 200;x<210;x++ {
			for y:= 200;y<210;y++ {
				env.PutValue(x,y)
			}
		}
		for i:=0;i<*numTurtles;i++ {
			turtles[i] = core.NewTurtle(env, &actors.Ant{SniffDistance:3, Carrying:false})
		}
	case "woodchips":
		for x := 0;x<*numTurtles;x++ {
			env.PutValue(rand.Intn(*width), rand.Intn(*height))
		}

		for x:= 200;x<210;x++ {
			for y:= 200;y<210;y++ {
				env.PutValue(x,y)
			}
		}
		for i:=0;i<*numTurtles;i++ {
			turtles[i] = core.NewTurtle(env, &actors.WoodChips{SniffDistance:20, Carrying:false})
		}
	case "slimemold":
		for i:=0;i<*numTurtles;i++ {
			turtles[i] = core.NewTurtle(env, &actors.SlimeMold{SniffDistance:20})
		}
	default:
		for i:=0;i<*numTurtles;i++ {
			turtles[i] = core.NewTurtle(env, &actors.SlimeMold{SniffDistance:20})
		}
	}



	g := graphics.NewGraphics(int32(*width),int32(*height))

	running := true
	wait := sync.WaitGroup{}

	wait.Add(1)
	go func() {
		for running {
			g.Render(env, turtles)
		}
		wait.Done()
	}()


	wait.Add(1)
	for running {
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch event.(type) {
			case *sdl.QuitEvent:
				running = false
				break
			}
		}
	}
	wait.Done()
	wait.Wait()
}

