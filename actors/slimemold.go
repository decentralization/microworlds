package actors

import (
	"git.openprivacy.ca/sarah/microworlds/core"
)

type SlimeMold struct {
	SniffDistance int
}

func(sm * SlimeMold) Setup(env *core.Environment, t *core.Turtle) {
	// Do nothing
}

func (sm * SlimeMold) Run(env *core.Environment, t *core.Turtle) {
	t.Wiggle()
	t.FollowGradient(env, sm.SniffDistance, 2)
	t.Step(env)
	t.Drop(env, 1)
}
