package actors

import (
	"git.openprivacy.ca/sarah/microworlds/core"
)

type WoodChips struct {
	SniffDistance int
	Carrying bool
}

func (a * WoodChips) Setup(env *core.Environment, t *core.Turtle) {
	//t.SetXY(150,150)
}

func (a*WoodChips) Run(env *core.Environment, t *core.Turtle) {
	if a.Carrying {
		if env.HasValue(t.Pos()) {
			for  {
				t.Wiggle()
				t.Step(env)
				if !env.HasValue(t.Pos()){
					env.PutValue(t.Pos())
					a.Carrying = false
					break
				}
			}
		}
	} else {
		if env.HasValue(t.Pos()) {
			env.TakeValue(t.Pos())
			a.Carrying = true
			t.TurnAround()
		}
	}
	t.Wiggle()
	t.Step(env)
}