package actors

import (
	"git.openprivacy.ca/sarah/microworlds/core"
)


type Ant struct {
	SniffDistance int
	Carrying bool
	DropSize float32
}

func (a * Ant) Setup(env *core.Environment, t *core.Turtle) {
	//t.SetXY(150,150)
}

func (a*Ant) Run(env *core.Environment, t *core.Turtle) {
	if a.Carrying == false {
		if env.HasValue(t.Pos()) {
			env.TakeValue(t.Pos())
			a.Carrying = true
			a.DropSize = 100
			t.TurnAround()
		} else {
			t.Wiggle()
			t.FollowGradient(env, a.SniffDistance, 5)
		}
		t.Step(env)
	} else if a.Carrying  == true {
		a.DropSize -= 0.6
		t.Drop(env, a.DropSize)
		t.Wiggle()
		t.Step(env)
	}
}

