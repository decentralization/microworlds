package core

type Actor interface {
	Setup(*Environment, *Turtle)
	Run(*Environment, *Turtle)
}
