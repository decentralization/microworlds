package core

import (
	"testing"
)

func TestEnvironment_Mark(t *testing.T) {
	env := NewEnvironment(3,3)
	env.Mark(1,1,9)
	env.Evaporate()
	for x:=0;x<3;x++ {
		for y:=0;y<3;y++ {
			t.Logf("mark(%d,%d) = %f", x,y,env.Sniff(x,y))
		}
	}

	t.Logf("\n")
	env.Evaporate()
	for x:=0;x<3;x++ {
		for y:=0;y<3;y++ {
			t.Logf("mark(%d,%d) = %f", x,y,env.Sniff(x,y))
		}
	}
}
