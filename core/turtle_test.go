package core

import (
	"testing"
)

func TestTurtle_Wiggle(t *testing.T) {
	turtle := NewTurtle(NewEnvironment(3,3,))
	counts := []int{0,0,0,0,0,0,0,0}
	for i:=0;i<1000;i++ {
		turtle.Wiggle()
		//t.Logf("Heading %v", turtle.heading)
		counts[turtle.heading]++
	}
	t.Logf("Heading %v", counts)
}